from random import randint
from matplotlib import pyplot

#Lines used to fill the test array
#file = open("./testarray", mode="w")

#array_line = ""
#for i in range(500):
#    array_line += str(randint(0, int(500)))+" "
#file.write(array_line)


file_path = input("Please insert the file path:")
file = open(file_path)
line = file.readline()
str_value_array = line.strip().split(' ')

if len(str_value_array) == 1:
    if str_value_array[0] == '':
        print("Peaks: 0 Valleys: 0")
        exit()
    # If the array length is 1, that element is a peak and a valley, because its not smaller nor greater
    # Than the empty set of neighbors
    else:
        print("Peaks: 1 Valleys: 1")
        exit()


#Appends a value the value before the last one to the end of the array,
#To be able to calculate the discrete second derivative in the right corner
str_value_array.append(str_value_array[len(str_value_array)-2])
left_value = int(str_value_array[1])
center_value = int(str_value_array[0])
peaks_num = 0
valleys_num = 0
num_array = list()

for value in str_value_array[1:]:
    num_array.append(center_value)
    right_value = int(value)
    if center_value >= left_value and center_value >= right_value:
        peaks_num += 1
    if center_value <= left_value and center_value <= right_value:
        valleys_num += 1
    left_value = center_value
    center_value = right_value

print("Peaks: "+str(peaks_num)+" Valleys: "+str(valleys_num))

#Shows the array analized
pyplot.plot(num_array)
pyplot.show()