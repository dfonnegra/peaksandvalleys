# PeaksAndValleys

Simple implementation of the solution to the following problem:

Given an array of integers with length N. An array element is peak if it is NOT smaller than its
neighbors. An array element is valley if it is NOT greater than its neighbors. For corner elements,
we need to consider only one neighbor. We need to return the total number of
peaks and valleys.

## Implementation
The program finds the number of peaks and valleys defined before in a given array. In the first
step the program asks the user for the file path of the input array, it must contain a single line
with N numbers separated by a space character.

Then, the program traverses the array and verifies for each point if it's a peak, a valley, or both.
This is done by verifying for each (left, center, right) tuple if the center is 
not smaller than left and right (for peaks), or equivalently if
```
center >= left and center >= right
```
and if the center is not greater than left and right (for valleys).

For dealing with the left corner, the program starts the traversing with a left value equal to the
right value, and adds an element to the end of the array with the same value as the penultimate value.   

Finally, the program prints the number of peaks and valleys and shows a plot with the given array which
helps to visualize the data.

This implementation was chosen because it could be implemented while the program was reading the
input array, so it doesn't consumes additional computation time. 

